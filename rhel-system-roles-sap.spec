%if 0%{?rhel} && ! 0%{?epel}
%bcond_with ansible
%else
%bcond_without ansible
%endif

%bcond_with collection_artifact

%if 0%{?fedora} || 0%{?rhel} >= 8
%bcond_without html
%else
# pandoc is not supported in rhel 7 and older,
# which is needed for converting .md to .html.
%bcond_with html
%endif

%global collection_name sap_install
%if 0%{?rhel}
%define package_name rhel-system-roles-sap
%global collection_namespace redhat
%else
%define package_name linux-system-roles-sap
%global collection_namespace community
%endif

# collection_version has to be increased by every rebuild
# otherwise, it cannot be uploaded to Automation Hub and Galaxy
# due to version conflict
%define collection_version 1.0.1

%global mainid d64d1be7904aa8f279b117da6f2a31b3d4b6e43e
%global commit_id_sap_install d8330042de222480c78e040846db8bffe4321cf4
%global rolename1 sap_general_preconfigure
%global rolename2 sap_hana_preconfigure
%global rolename3 sap_netweaver_preconfigure
%global rolename4 sap_hana_install
%global src_owner linux-system-roles
%global github_repo_sap_install community.sap_install

Name: %{package_name}
Summary: System Roles to configure RHEL for running SAP NetWeaver- or SAP HANA-based products
Version: 3.2.0
Release: 2%{?dist}
License: GPLv3+
Url: https://github.com/berndfinger/community.sap_install
Source0: https://github.com/linux-system-roles/auto-maintenance/archive/%{mainid}/auto-maintenance-%{mainid}.tar.gz
Source1: https://github.com/berndfinger/%{github_repo_sap_install}/archive/%{commit_id_sap_install}/%{github_repo_sap_install}-%{commit_id_sap_install}.tar.gz
Source4: README.md
Source5: galaxy.yml
# patches 0-49 for sap-preconfigure
# patches 50-99 for sap-hana-preconfigure
Patch50: sap-hana-preconfigure-md2html-error.patch
# patches 100-149 for sap-netweaver-preconfigur

BuildArch: noarch


%if %{with html}
# Requirements for md2html.sh to build the documentation
%if 0%{?fedora} || 0%{?rhel} >= 9
BuildRequires: rubygem-kramdown-parser-gfm
%else
BuildRequires: pandoc
BuildRequires: asciidoc
BuildRequires: highlight
%endif
%endif

# Requirements for galaxy_transform.py
BuildRequires: python3
%if 0%{?fedora} || 0%{?rhel} >= 8
BuildRequires: python3dist(ruamel.yaml)
Requires: python3-jmespath
Requires: python3-netaddr
%else
BuildRequires: python3-ruamel-yaml
Requires: python-jmespath
Requires: python-netaddr
%endif

%if %{with ansible}
BuildRequires: ansible-core >= 2.9.10
%endif

%if %{undefined __ansible_provides}
Provides: ansible-collection(%{collection_namespace}.%{collection_name}) = %{version}
%endif
# be compatible with the usual Fedora Provides:
Provides: ansible-collection-%{collection_namespace}-%{collection_name} = %{version}-%{release}

%if 0%{?fedora} || 0%{?rhel} >= 8
%{!?ansible_collection_files:%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/}
%else
%if %{?ansible_collection_files:0}%{!?ansible_collection_files:1}
%define ansible_collection_files %{_datadir}/ansible/collections/ansible_collections/%{collection_namespace}/
%endif
%endif


%if %{without ansible}
%define ansible_collection_build_install() tar -cf %{_tmppath}/%{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz .; mkdir -p %{buildroot}%{ansible_collection_files}%{collection_name}; (cd %{buildroot}%{ansible_collection_files}%{collection_name}; tar -xf %{_tmppath}/%{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz)
%else
%define ansible_collection_build_install() ansible-galaxy collection build; ansible-galaxy collection install -n -p %{buildroot}%{_datadir}/ansible/collections %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz
%endif

%description
Collection of Ansible roles which configures a RHEL system according
to applicable SAP notes so that any SAP software can be installed.

%if %{with collection_artifact}
%package collection-artifact
Summary: Collection artifact to import to Automation Hub / Ansible Galaxy

%description collection-artifact
Collection artifact for %{name}. This package contains %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz
%endif

%prep
%setup -q -a1 -n auto-maintenance-%{mainid}

# cp own galaxy.yml and rename the collection name correctly for Automation Hub and Galaxy
mkdir .collections
cp %{SOURCE5} galaxy.yml
sed -i -e "s|namespace: redhat|namespace: %{collection_namespace}|" galaxy.yml
# add correct version for Automation Hub and Galaxy
sed -i -e "s|version: 1.0.0|version: %{collection_version}|" galaxy.yml

pushd %{github_repo_sap_install}-%{commit_id_sap_install}/roles/sap_hana_preconfigure
%patch50 -p1
popd

pushd %{github_repo_sap_install}-%{commit_id_sap_install}/roles/
# remove zero file
rm -f sap_hana_preconfigure/tasks/RedHat_6/recommendations.yml
# remove all SLES stuff and RHEL 6 stuff
rm -rf sap_hana_preconfigure/tasks/SLES/
rm -rf sap_hana_preconfigure/tasks/sapnote/1275776 # SLES - sapconf, saptune, etc
rm -rf sap_hana_preconfigure/tasks/sapnote/1944799 # SLES - HANA install and config guidelines
rm -f  sap_hana_preconfigure/tasks/sapnote/1944799.yml
rm -f  sap_hana_preconfigure/tasks/sapnote/2013638.yml # RHEL 6.5 - OS settings
rm -f  sap_hana_preconfigure/tasks/sapnote/2136965.yml # RHEL 6.6 - OS settings
rm -f  sap_hana_preconfigure/tasks/sapnote/2247020.yml # RHEL 6.7 - OS settings
rm -rf sap_hana_preconfigure/tasks/sapnote/2578899 # SLES 15 - installation
rm -rf sap_hana_preconfigure/tasks/sapnote/2684254 # SLES 15 - OS settings for HANA
rm -f  sap_hana_preconfigure/vars/RedHat_6.5.yml
rm -f  sap_hana_preconfigure/vars/RedHat_6.6.yml
rm -f  sap_hana_preconfigure/vars/RedHat_6.7.yml
rm -f  sap_hana_preconfigure/vars/SLES_15.yml
popd

%build
sh md2html.sh \
%{github_repo_sap_install}-%{commit_id_sap_install}/roles/sap_general_preconfigure/README.md \
%{github_repo_sap_install}-%{commit_id_sap_install}/roles/sap_hana_preconfigure/README.md \
%{github_repo_sap_install}-%{commit_id_sap_install}/roles/sap_netweaver_preconfigure/README.md

for role in %{rolename1} %{rolename2} %{rolename3} %{rolename4} ; do
    new_role="$(echo ${role/sap-/} | tr '-' '_')"
    python3 lsr_role2collection.py --role "$role" \
        --src-path %{github_repo_sap_install}-%{commit_id_sap_install}/roles/"$role" \
        --src-owner %{src_owner} \
        --dest-path .collections \
        --readme %{SOURCE4} \
        --namespace %{collection_namespace} \
        --collection %{collection_name} \
        --new-role "$new_role" \
        --extra-mapping "timesync:redhat.rhel_system_roles.timesync,storage:redhat.rhel_system_roles.storage,sap-preconfigure:sap.rhel.preconfigure,sap-hana-preconfigure:sap.rhel.hana_preconfigure,sap-netweaver-preconfigure:sap.rhel.netweaver_preconfigure" 
done

rm -f .collections/ansible_collections/%{collection_namespace}/%{collection_name}/tests/sanity/ignore-2.9.txt
# Merge .sanity-ansible-ignore-2.9-ROLENAME.txt into tests/sanity/ignore-2.9.txt
for role in %{rolenames}; do
    if [ -f .collections/ansible_collections/%{collection_namespace}/%{collection_name}/.sanity-ansible-ignore-2.9-"$role".txt ];
    then
      cat .collections/ansible_collections/%{collection_namespace}/%{collection_name}/.sanity-ansible-ignore-2.9-"$role".txt \
        >> .collections/ansible_collections/%{collection_namespace}/%{collection_name}/tests/sanity/ignore-2.9.txt
      rm -f .collections/ansible_collections/%{collection_namespace}/%{collection_name}/.sanity-ansible-ignore-*-"$role".txt
    fi
done

# removing dot files/dirs
rm -rf .collections/ansible_collections/%{collection_namespace}/%{collection_name}/.[A-Za-z]*

cp -p galaxy.yml .collections/ansible_collections/%{collection_namespace}/%{collection_name}

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/ansible/roles \
         $RPM_BUILD_ROOT%{_pkgdocdir}/sap_hana_preconfigure \
         $RPM_BUILD_ROOT%{_pkgdocdir}/sap_general_preconfigure \
         $RPM_BUILD_ROOT%{_pkgdocdir}/sap_netweaver_preconfigure \
         $RPM_BUILD_ROOT%{_pkgdocdir}/sap_hana_install

pushd %{github_repo_sap_install}-%{commit_id_sap_install}/roles/
for f in sap_hana_preconfigure sap_general_preconfigure sap_netweaver_preconfigure sap_hana_install ; do
  cp -pR $f $RPM_BUILD_ROOT%{_datadir}/ansible/roles/
  install -m 644 ../LICENSE $f/README.md $RPM_BUILD_ROOT%{_pkgdocdir}/$f
done
popd

# install collection files
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
%ansible_collection_build_install
popd

# install collection_artifact
%if %{with collection_artifact}
# Copy collection artifact to /usr/share/ansible/collections/ for collection-artifact
pushd .collections/ansible_collections/%{collection_namespace}/%{collection_name}/
mv %{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz $RPM_BUILD_ROOT%{_datadir}/ansible/collections/
popd
%endif

%files
%dir %{_pkgdocdir}
%dir %{_pkgdocdir}/sap_general_preconfigure
%dir %{_pkgdocdir}/sap_hana_preconfigure
%dir %{_pkgdocdir}/sap_netweaver_preconfigure
%dir %{_pkgdocdir}/sap_hana_install
%dir %{_datadir}/ansible
%dir %{_datadir}/ansible/roles
%doc %{_pkgdocdir}/*/README.md
%license %{_pkgdocdir}/*/LICENSE
%{_datadir}/ansible/roles/*
%{ansible_collection_files}

%if %{with collection_artifact}
%files collection-artifact
%{_datadir}/ansible/collections/%{collection_namespace}-%{collection_name}-%{collection_version}.tar.gz
%endif

%changelog
* Tue Feb 22 2022 Than Ngo <than@redhat.com> - 3.2.0-2
- Related: #2047282, new rebase, fixed collection basename

* Mon Jan 31 2022 Than Ngo <than@redhat.com> - 3.2.0-1
- Resolves: #2047282, Rebase to new upstream org sap-linuxlab

* Fri Dec 03 2021 Than Ngo <than@redhat.com> - 3.1.1-15
- Related: #2021579, ansible-lint cleanup

* Tue Nov 16 2021 Than Ngo <than@redhat.com> - 3.1.1-14
- Resolves: #2021579, ansible-lint cleanup

* Fri Oct 15 2021 Than Ngo <than@redhat.com> - 3.1.1-13
- Fix the version of the collection tar file

* Fri Oct 15 2021 Than Ngo <than@redhat.com> - 3.1.1-12
- Resolves: #2003630, Pepare the roles for ansible-core 2.11: Only use ansible.core modules

* Wed Sep 01 2021 Than Ngo <than@redhat.com> - 3.1.1-11
- fix yum group install errors on RHEL 7
- use grubby for setting tsx=on
- set min kernel version for RHEL 8.4 x86_64

* Fri Aug 20 2021 Than Ngo <than@redhat.com> - 3.1.1-10
- Related: #1977195, fix #149, #177, #193, #194

* Tue Aug 10 2021 Mohan Boddu <mboddu@redhat.com> - 3.1.1-9
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Fri Jul 16 2021 Than Ngo <than@redhat.com> - 3.1.1-8
- Related: #1977195, add missing vars/RedHat_9.yml in sap-netweaver-preconfigure

* Tue Jul 13 2021 Than Ngo <than@redhat.com> - 3.1.1-7
- Related: #1977195, add missing __sap_preconfigure_envgroups for RHEL 7.x and
  missing __sap_hana_preconfigure_req_repos variables for RHEL 8.5 and later

* Tue Jun 29 2021 Than Ngo <than@redhat.com> - 3.1.1-6
- Resolves: #1977195, make RHEL system roles for SAP RHEL9 compatible

* Mon Jun 28 2021 Than Ngo <than@redhat.com> - 3.1.1-5
- updated tarball, galaxy.yml, README.md

* Mon Jun 07 2021 Than Ngo <than@redhat.com> - 3.1.1-4
- update README.md
- Adding the -collection-artifact subpackage, enabled using
  "--with collection_artifact". It is used for importing to
  ansible galaxy/automation hub.

* Fri May 21 2021 Than Ngo <than@redhat.com> - 3.1.1-3
- convert roles to FQCN in the README files

* Thu May 20 2021 Than Ngo <than@redhat.com> - 3.1.1-2
- rename dashes by underscores and drop sap-

* Tue May 18 2021 Than Ngo <than@redhat.com> - 3.1.1-1
- rebase

* Wed Apr 14 2021 Than Ngo <than@redhat.com> - 3.1.0-3
- bz#1928398, rebuilt against new python-ruamel-yaml-0.15.100 for conversion problem

* Mon Mar 22 2021 Than Ngo <than@redhat.com> - 3.1.0-2
- rebase

* Tue Mar 02 2021 Than Ngo <than@redhat.com> - 3.1.0-1
- added support Transfer SAP roles into collection format

* Wed Feb 24 2021 Than Ngo <than@redhat.com> - 3.0.1-3
- Related: #1891642, fixed issue #125 in sap-preconfigure and #163 and #164 in sap-hana-preconfigure

* Thu Feb 18 2021 Than Ngo <than@redhat.com> - 3.0.1-2
- Related: #1891642, update sap-hana-preconfigure 3.0.1 and fixed Urls

* Thu Feb 18 2021 Than Ngo <than@redhat.com> - 3.0.1-1
- Related: #1891642, rebase to 3.0.1

* Mon Feb 15 2021 Than Ngo <than@redhat.com> - 3.0.0-2
- Related: #1891642, more fixes #145, #148, #149, #150 #147

* Thu Jan 28 2021 Than Ngo <than@redhat.com> - 3.0.0-1
- Resolves: #1891638, bug fixes
- Resolves: #1891642, new features

* Tue Jan 12 2021 Than Ngo <than@redhat.com> - 2.0.0-8
- RHEL 9.0.0 Alpha build

* Mon Sep 21 2020 Than Ngo <than@redhat.com> - 2.0.0-7
- Related: #1867027, fix #119

* Thu Aug 20 2020 Than Ngo <than@redhat.com> - 2.0.0-6
- Related: #1867027, fix #92

* Fri Aug 07 2020 Than Ngo <than@redhat.com> - 2.0.0-5
- Resolves: #1867027, more fixes for #89, #109, #110, #111, #112, #113, #114, #116

* Tue Jul 21 2020 Than Ngo <than@redhat.com> - 2.0.0-4
- Related: #1844190, more fixes for #84, #85, #86, #88, #98, #104, #105, #107

* Fri Jul 03 2020 Than Ngo <than@redhat.com> - 2.0.0-3
- Related: #1844190, drop 1944799.yml

* Mon Jun 29 2020 Than Ngo <than@redhat.com> - 2.0.0-2
- Related: #1844190, more fixes for #78, #79, #81, #82, #94-#97, #99, #101-#102

* Mon Jun 08 2020 Than Ngo <than@redhat.com> - 2.0.0-1
- Resolves: #1844190, rebase to 2.0.0

* Thu May 07 2020 Than Ngo <than@redhat.com> - 1.1.1-2
- Resolves: #1829495, fix summary

* Mon Oct 21 2019 Than Ngo <than@redhat.com> - 1.1.1-1
- rebase to 1.1.1
- always use ansible_hostname + ansible_domain instead of ansible_fqdn
- use {{ ansible_hostname }}.{{ ansible_domain }} instead of {{ ansible_fqdn }}
- added LC_ALL=C before commands for which the outputs are to be evaluated

* Thu Sep 12 2019 Than Ngo <than@redhat.com> - 1.1-1
- rebase to 1.1

* Fri Jun 28 2019 Than Ngo <than@redhat.com> - 1.0-1
- Initial release.
